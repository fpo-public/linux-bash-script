clear

# update & upgrade
echo "----------Update and Install epel-release CentOS----------"
yum -y update
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*
yum -y install epel-release

echo "----------Install nano editor----------"
yum -y install nano
yum -y install git

echo "----------Install MySQL / MariaDB----------"
yum -y install mariadb-server mariadb
systemctl start mariadb.service
systemctl enable mariadb.service
mysql_secure_installation

echo "----------Apache 2.4----------"
yum -y install httpd
systemctl start httpd.service
systemctl enable httpd.service

echo "----------Install firewall-cmd----------"
yum -y install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
sudo systemctl status firewalld
firewall-cmd --permanent --zone=public --add-service=http 
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload

echo "----------Install PHP 7.2----------"
rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum -y install yum-utils
yum -y update
yum-config-manager --enable remi-php72
yum -y install php php-opcache
systemctl restart httpd.service

echo "Created PHP test file ! "
echo "<?php phpinfo(); ?>" > /var/www/html/info.php

echo "----------Install some common PHP modules ----------"
yum -y install php-gd php-ldap php-odbc php-pear php-xml php-xmlrpc php-mbstring php-soap curl curl-devel
systemctl restart httpd.service

echo "----------Install phpMyAdmin----------"
yum -y install phpMyAdmin
nano /etc/httpd/conf.d/phpMyAdmin.conf
systemctl restart httpd.service


echo "Install LAMP Server successfully !!!"