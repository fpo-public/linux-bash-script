clear

echo "----------Install NodeJS----------"
cd ~
curl -sL https://deb.nodesource.com/setup_11.x -o nodesource_setup.sh

bash nodesource_setup.sh
apt install nodejs

echo "Install Node.JS successfully. Your version is: $(nodejs -v)"


echo "----------Install MongoDB----------"
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list

apt update

apt install -y mongodb-org

echo "----------Start MongoDB service----------"
ufw enable
ufw allow ssh
ufw allow 27017
systemctl enable mongod
systemctl start mongod 