clear

# update & upgrade
echo "----------Update and Upgrade Ubuntu----------"
sudo apt-get update
sudo apt-get upgrade -y

# install apache2
echo "----------Install Apache2----------"
sudo apt install apache2 -y

# enable & config Firewall
sudo ufw enable
sudo ufw allow in "Apache Full"
sudo ufw allow ssh
sudo ufw allow 3306

# install PHP
echo "----------Install PHP 7.0----------"
# install PHP
sudo apt install php7.0 php7.0-cli php7.0-common libapache2-mod-php -y

# install some php extensions
sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
sudo phpenmod mbstring
sudo a2enmod rewrite
php -v

# install MySQL
echo "----------Install MySQL Server----------"
sudo apt install mysql-server -y
sudo apt install php7.0-mysql -y

# install phpMyAdmin
echo "----------Install phpMyAdmin----------"
sudo apt install phpmyadmin -y

# create PHP test file
echo "Created PHP test file ! "
echo "<?php phpinfo(); ?>" > /var/www/html/info.php

# update PHP config
echo "Update PHP config "
echo "<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>" > /etc/apache2/mods-enabled/dir.conf

echo "<Directory /var/www/html/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>

ServerName localhost" >> /etc/apache2/apache2.conf

# Restart and show status of Apache2
service apache2 restart
service apache2 status

echo "Install LAMP Server successfully !!!"

clear
echo "----------Download and Install Wordpress----------"
sudo rm -rf /var/www/html/
cd /var/www/
sudo curl -O https://wordpress.org/latest.tar.gz
sudo tar xzvf latest.tar.gz
sudo cp ./wordpress/wp-config-sample.php ./wordpress/wp-config.php
mv ./wordpress ./html

sudo chown -R www-data:www-data /var/www/html
sudo find /var/www/html/ -type d -exec chmod 750 {} \;
sudo find /var/www/html/ -type f -exec chmod 640 {} \;
sudo chmod 755 /var/www/html

echo "define('FS_METHOD', 'direct');" >> /var/www/html/wp-config.php
sudo curl -s https://api.wordpress.org/secret-key/1.1/salt/