sudo cp -r ./* ~/
cd ~

# yum update
sudo yum update -y

# Download Prometheus for Linux - amd64
wget -O prometheus.tar.gz https://github.com/prometheus/prometheus/releases/download/v2.28.0/prometheus-2.28.0.linux-amd64.tar.gz
tar xvzf prometheus.tar.gz

# Configuring Prometheus as a service
sudo useradd -rs /bin/false prometheus
cd prometheus*
sudo cp prometheus promtool /usr/local/bin
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo mkdir /etc/prometheus
sudo cp -R consoles/ console_libraries/ prometheus.yml /etc/prometheus
sudo mkdir -p /data/prometheus
sudo chown -R prometheus:prometheus /data/prometheus /etc/prometheus/*

sudo cp ~/prometheus.service /lib/systemd/system/prometheus.service

sudo systemctl enable prometheus
sudo systemctl start prometheus
sudo systemctl status prometheus

# Config firewall
sudo ufw allow 9090

echo "Install Prometheus completed !"
echo "-------------------------------------------------------------"

# Download Grafana
sudo cp ~/grafana.repo /etc/yum.repos.d/grafana.repo

# yum update
sudo yum update -y

# Install Grafana with YUM
sudo yum install grafana -y

# Start your grafana-server service
sudo systemctl enable grafana-server
sudo systemctl start grafana-server
sudo systemctl status grafana-server

# Config firewall
sudo ufw allow 3000

echo "Install Grafana completed !"
echo "-------------------------------------------------------------"

# Downloading the Node Exporter
cd ~
sudo wget -O node_exporter.tar.gz https://github.com/prometheus/node_exporter/releases/download/v1.1.2/node_exporter-1.1.2.linux-amd64.tar.gz
tar xvzf node_exporter.tar.gz
cd node_exporter*
sudo cp node_exporter /usr/local/bin
sudo useradd -rs /bin/false node_exporter
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter

# Create a Node Exporter service
sudo cp ~/node_exporter.service /lib/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
sudo systemctl status node_exporter

# Config firewall
sudo ufw allow 9100

echo "Install Node_Exporter completed !"
echo "TODO: "
echo "- Add 'localhost:9100' to static_configs ==> targets in /etc/prometheus/prometheus.yml"
echo "- Restart prometheus"
echo "-------------------------------------------------------------"

# Download and Install Pushgateway
cd ~
wget -O pushgateway.tar.gz https://github.com/prometheus/pushgateway/releases/download/v1.4.1/pushgateway-1.4.1.linux-amd64.tar.gz
tar xvzf pushgateway.tar.gz
cd pushgateway*
./pushgateway & 

# Config firewall
sudo ufw allow 9091
echo "Install Pushgateway completed !"
echo "TODO: "
echo "- Add 'localhost:9091' to static_configs ==> targets in /etc/prometheus/prometheus.yml"
echo "- Restart prometheus"
echo "-------------------------------------------------------------"