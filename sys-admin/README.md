# SYS-ADMIN SCRIPTS
### *All command should be run with SUDO Permission*
### Tested on Ubuntu 18.04
-----------------------------

## Description
Simple menu driven shell script to to get information about your Linux server / desktop and Do some Users and File operations Quickly.

## Run
Just run `setup.sh` and have coffee ☕☕☕


## Reference
Sathish Arthar (Sathisharthar@gmail.com) - Jan 2014
Github: https://github.com/sathisharthar/Admin-Scripts
