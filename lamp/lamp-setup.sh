clear

# update & upgrade
echo "----------Update and Upgrade Ubuntu----------"
sudo apt-get update
sudo apt-get upgrade -y

# install apache2
echo "----------Install Apache2----------"
sudo apt install apache2 -y

# enable & config Firewall
sudo ufw enable
sudo ufw allow in "Apache Full"
sudo ufw allow ssh
sudo ufw allow 3306

# install PHP
echo "----------Install PHP 7.2----------"
# install PHP
sudo apt install php7.2 php7.2-cli php7.2-common libapache2-mod-php -y

# install some php extensions
sudo apt-get install php7.2-curl php7.2-gd php7.2-json php7.2-mbstring php7.2-gettext php7.2-intl php7.2-xml php7.2-zip -y
sudo phpenmod mbstring
php -v

# install MySQL
echo "----------Install MySQL Server----------"
sudo apt install mysql-server -y
sudo apt install php7.2-mysql -y

# install phpMyAdmin
echo "----------Install phpMyAdmin----------"
sudo apt install phpmyadmin -y

# create PHP test file
echo "Created PHP test file ! "
echo "<?php phpinfo(); ?>" > /var/www/html/info.php

# update PHP config
echo "Update PHP config "
echo "<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>" > /etc/apache2/mods-enabled/dir.conf

echo "<Directory /var/www/html/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>

ServerName localhost" >> /etc/apache2/apache2.conf

sudo a2enmod rewrite

# Restart and show status of Apache2
service apache2 restart
service apache2 status

# Fix phpMyAdmin Issue
echo "----------Fix phpMyAdmin Issue----------"
cp lib/plugin_interface.lib.php /usr/share/phpmyadmin/libraries/plugin_interface.lib.php
cp lib/sql.lib.php /usr/share/phpmyadmin/libraries/sql.lib.php

echo "Install LAMP Server successfully !!!"