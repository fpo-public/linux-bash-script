#!/bin/bash
echo "Project name (fpo,vn, childhub, pickup, . . . )": 
read projectName

echo "Subdomain (Ex: childhub.fpo.vn)":
read subDomain

# Change permission in /var/www/{projectName} directory
sudo chown -R www-data:www-data /var/www/$projectName
sudo chmod -R 775 /var/www/$projectName

# Create VirtualHost
sudo cp ./vhost.conf /etc/apache2/sites-available/$projectName.conf
cd /etc/apache2/sites-available
sudo sed -i "s|projectName|${projectName}|g" $projectName.conf
sudo sed -i "s|subdomain|${subDomain}|g" $projectName.conf

# Enable site
sudo a2ensite $projectName.conf
sudo systemctl reload apache2